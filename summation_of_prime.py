def summation_of_prime(upper_limt):
    i = 5
    primes = [2,3]
    while i < upper_limt:
        is_prime = True
        stop = i ** 0.5 
        for prime in primes:
            if i % prime == 0:
                is_prime = False
                break
            if prime > stop:
                break
        if is_prime:
            primes.append(i)
        i += 2
    return sum(primes)

print(summation_of_prime(2000000))